package pt.ulusofona.sme.watchtasklist;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.wear.widget.WearableLinearLayoutManager;
import android.support.wear.widget.WearableRecyclerView;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.TextView;

public class ListActivity extends WearableActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        WearableRecyclerView mWearableRecyclerView = (WearableRecyclerView)this.findViewById(R.id.recycler_launcher_view);

        // To align the edge children (first and last) with the center of the screen
        mWearableRecyclerView.setEdgeItemsCenteringEnabled(true);

        mWearableRecyclerView.setLayoutManager(new WearableLinearLayoutManager(this));

        MyAdapter adapter = new MyAdapter(new TaskListProvider().getList());
        mWearableRecyclerView.setAdapter(adapter);

    }

}
